package main

import (
	"encoding/json"
	"fmt"
	"os"
	"sync"
)

type User struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
}

var (
	users []User
	m     sync.Mutex
)

func (u *User) CreateUser(wg *sync.WaitGroup) {
	m.Lock()
	defer m.Unlock()

	username := fmt.Sprintf("user%d", u.Id)
	email := fmt.Sprintf("user%d@xyz.com", u.Id)
	newUser := User{
		Id:       u.Id,
		Username: username,
		Email:    email,
	}

	users = append(users, newUser)

	data, err := json.MarshalIndent(users, "", "   ")
	if err != nil {
		fmt.Println("Error marshaling json data", err)
		return
	}

	err = os.WriteFile("users.json", data, 0644)
	if err != nil {
		fmt.Println("Error writing to file", err)
	}
	defer wg.Done()
}

func main() {
	var w sync.WaitGroup

	for i := 0; i < 10; i++ {
		w.Add(1)
		user := User{
			Id: i + 1,
		}
		go user.CreateUser(&w)
	}
	w.Wait()
}
